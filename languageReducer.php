<?php

// Started with: https://github.com/manassharma07/English-Dictionary-CSV/
// To make a properly categorized csv dictionary
// And from there, an LCD dictionary.

// Commented out code is steps to get from original dictionary to resulting dictionary

// every_entry('fix_quotes', []);
// every_entry('seperate', []);


function every_entry($function, $args){
  $dict_dir = dirname(__FILE__)."/dict";
  $dir = new DirectoryIterator($dict_dir);
  foreach ($dir as $fileinfo) {
      if (!$fileinfo->isDot()) {
        $filename = $fileinfo->getFilename();
        update_file($dict_dir, $filename, $function, $args);
      }
  }
}

function fix_quotes($line){
  echo "Fixing Quotes \n";
  $line = trim(preg_replace('/\s\s+/', ' ', $line));
  $line = '"' . trim($line, '"') . '"';
  if ($line == '""'){
    $line = "";
  } else {
    $line = $line . PHP_EOL;
  }
  return $line;
}

function seperate($line){
  $line = trim(preg_replace('/\s\s+/', ' ', $line));
  $line = trim($line, '"');
  $line_ex = explode("(", $line, 2);
  $line_ex[0] = trim($line_ex[0], " ");
  $line = implode('", "', $line_ex);
  $line_ex_2 = explode(")", $line, 2);
  $line_ex_2[1] = trim($line_ex_2[1], " ");
  $line_ex_2[1] = str_replace('"', '""', $line_ex_2[1]);
  $line = '"' . implode('", "', $line_ex_2) . '"' . PHP_EOL;
  return $line;
}

function update_file($dic, $file, $operation, $args){
  $source = $dic . "/" . $file;
  $target = $dic . "/out.csv";

  // copy operation
  $sh=fopen($source, 'r');
  $th=fopen($target, 'w');
  echo "Working on " . $source . "\n";
  rewind($sh);
  while (!feof($sh)) {
      $line=fgets($sh);
      echo "converting line: " . $line . "\n";
      $line = $operation($line);
      fwrite($th, $line);
  }

  fclose($sh);
  fclose($th);

  // delete old source file
  unlink($source);
  // rename target file to source file
  rename($target, $source);
}
